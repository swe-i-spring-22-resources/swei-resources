# Software Engineering I Spring 2022 Setup Guide
Written by Cristina Reyes and Keaton Rohwer

This guide will be updated with more information at a later date. We will provide links to new information when it is needed.

## Table of Contents
* [SWE Software Setup](#software-suite-setup)
* [DevOps Documentation](/devops/devops.md)
* [Full Stack Documentation](/fullstack/fullstack.md)
    * [Back-End Services](/fullstack/back-end/services.md)
* API Documentation (Coming Soon)
* Testing Documentation
    * [Black Box Testing](/testing/blackbox.md)
    * [White Box Testing](/testing/whitebox.md)
* [Version Control Documenation](/vcs)
    * [Git Documentation](/vcs/git.md)
    * [Pull Requests](/vcs/pullrequests.md)

## Software Suite Setup

### BitBucket Setup
For this course, you are **required** to create a student account with BitBucket.

1. Using your St. Edward's email, [sign up for a BitBucket Student Account](https://bitbucket.org/product/education).
2. Once your entire team has signed up with BitBucket, we will create a private workspace that your team will use for your repositories throughout the semester.

### Python Setup
If you don't already have Python installed on your device, you will need to install the latest version.

* [Python for Windows](https://www.python.org/downloads/windows/)
* [Python for MacOS](https://www.python.org/downloads/macos/)
* [Python for Linux](https://www.python.org/downloads/source/)

When installing with Windows, make sure to install Python globally (to all users) and check the option "Add Python to System PATH variable"

To make sure that the Python library and interpreter installed propertly, open up a new terminal, command prompt, or PowerShell window, and type `python3 -V` check which version you have installed.

### Git Setup
Before you're able to share code with your teammates through your repositories, you will need to install Git on your device. 

* [Git download for Windows, Mac, & Linux](https://git-scm.com/downloads)

You can then use the command `git --version` to make sure Git installed properly.

### IDEs
Here are the downloads for the two IDEs that we are recommending for this course.

* Front End Development: [Visual Studio Code](https://code.visualstudio.com/)
* Back End Development: [PyCharm](https://www.jetbrains.com/pycharm/)

For PyCharm, you can either get a student license, or the community version. Both will work perfectly fine for the purposes of this project.

### JetUML
JetUML is a program that allows you to easily create diagrams using the Unified Modeling Language (UML). Here are some helpful links to help set it up.

* [Download JetUML](https://github.com/prmr/JetUML/releases)
* [JetUML Installation Guide](https://www.jetuml.org/docs/install.html)
* [JetUML Main Page for more info](https://www.jetuml.org/)

## Documentation

### Documenting Code in Google Docs
If you need to include code samples in any documents posted to your google drive, here is a very helpful extension that allows you to include syntax highlighting and make your code samples look a lot better.

* [Code Blocks for Google Workspaces](https://workspace.google.com/marketplace/app/code_blocks/100740430168)

## Miscellaneous

### Additional Technical Help
If you ever run into any code problems, we highly recommend you look around on the following site to potentially solve your issue:

* [Stack Overflow Main Page](https://stackoverflow.com/)

We don't expect you to know and be capable of solving every issue by yourself, do some research so you can keep moving forward! If you've *still* hit a roadblock after a lot of research and troubleshooting, definitely reach out to your team and us for more help.