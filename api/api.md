# API Documentation
An API is an Application Programming Interface and it is responsible for defining resource transactions. APIs define how the client should format their request and what methods to use, as well as how the recipient of the request should respond.

A common type of API is a REST or Representational State Transfer API. APIs following REST should be set up as a uniform response given a uniform request.

* [Here is a resource](https://restfulapi.net/) if you want to learn more about REST APIs

## Postman
For this course, you'll be using Postman in order to define, document, and test your own APIs, as well as explore API calls to any external service that you will be using such as Map Services.

* [Postman](https://www.postman.com/)

I would recommend downloading the desktop app, though you can use it in your browser if you need.

## Creating a Team Workspace
Before getting started with APIs, you'll want to create a team workspace so that your team members can access your work and you can collaborate through Postman.

In the Postman app, there should be an option on the left side of the window to create a team. Select this and enter information to identify your team, and then you can invite your team members either by email or by providing an email link.

### Collections
Collections are a way of grouping requests to better organize your workspace. I would recommend creating collections based on the APIs that you want to test.

You can create a collection by either clicking "create collection" if you do not already have one, or by clicking "new" and selecting collection from the menu.

![Creating a collection](/api/screenshots/postmannewcollection.png)

## Testing an API in Postman
Testing an API in Postman is as simple as creating a request, sending the desired information, and then ensuring the response matches what you expect.

you can create a request by clicking "new" and selecting HTTP request from the menu.

You should then be met with a page with some empty fields. The first few are where you enter the name of your request, select the type of request you want to make (GET or POST), and finally the address or endpoint that you want to send the request to.

![request name, type, and location](/api/screenshots/apinameandaddress.png)

The next section is where  you can define your parameters. These can either be in the form of key-value pairs as shown, or for POST requests you can select "Body" and enter a post body in JSON.

![api parameters](/api/screenshots/apiparameters.png)

Finally, after you've written your request, you can hit the send button, and you should see the response show up in the bottom of the window. In the case of this example, I just sent a GET request to google, so the response that I receive is the HTML code of Google's homepage.

![api response](/api/screenshots/apiresponse.png)

You can then save this response if you want.

## Writing an API
Writing an API involves defining the endpoints that you want users to be able to request, any parameters that need to be sent with each request, and finally defining the response that is returned to the user given a request.

After an API is defined in postman, it can be used as a template to implement the API into the system.

To start, you must create an API through Postman by selecting the API section, and clicking "Create an API"

![create api](/api//screenshots/createapi.png)

In the window that pops up, you should give your API a descriptive name, like "Vehicle Request API," set a version number, and **be sure to set schema format to JSON.**

Next you will be met with a page that allows you to write a summary and description of the API. Don't worry about filling this out right away if you're not sure, it can be accessed again by clicking on the name of the API in the menu on the left.

Click on the version number under the API name to start working with the API itself.

From this page, if you click either "view schema" or the "Definintion" tab at the top, you'll be met with your sample schema like so:

![sample schema](/api/screenshots/sampleschema.png)

the organization structure on the left corresponds to the written schema on the left. For a quick summary:

* General: just some general info about the API
* Servers: the URL of the server(s) the API will be placed on
* Paths: this is where you place your API endpoints that users will request
    * This will also include the parameters and responses for the path
* Components/Schemas: API schemas are a form of metadata that tell you how your data is structured

The security sections can be ignored for now unless you want to research and implement them yourselves.

The default schema should have a sample of all of this if you're not sure how it needs to be structured, just make sure you do not leave any of the sample information once you are done.

## Documenting an API
Once you have written your API, you can click the documentation section, and hit create documentation. This should auto generate some documentation for the API.

If you wish to document your API somewhere other than postman, which is strongly recommended for TA scorecard and team use purposes, then you should make sure it includes all the relevant information.

You'll want to make sure you include:

* A description of your API
* API endpoint(s)
    * Type of request (GET or POST)
    * Data required in parameters/POST body, and the proper formatting of these values
* Example requests and responses for each endpoint