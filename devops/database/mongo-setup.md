# MongoDB Setup
Follow this tutorial if you have chosen MongoDB as your database of choice.

MongoDB is a NoSQL database solution, which means it does not require defining a schema or table before using the database. Instead, Mongo and other non-relational databases use key-value pairs in JSON or XML, which offers more flexibility than a relational database and allows users to store non-traditional formats such as images or movies in addition to the typical strings and numbers.

## Installation
Before installing anything, be sure to update your package manager:

* `sudo apt update && sudo apt upgrade`

Then, use `apt` to install MongoDB Community Edition:

Download the Public GPG key from Mongo Repository. You should get a response 'OK'

* `wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -`

Create a list file for MongoDB on Ubuntu Version 20

* `echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list`

Reload the local package database

* `sudo apt-get update`

Install the latest version of MongoDB

* `sudo apt-get install -y mongodb-org`

Pin this MongoDB Version so that it doesn't automatically upgrade packages when a new version becomes available (This is dangerous!)

* `echo "mongodb-org hold" | sudo dpkg --set-selections && echo "mongodb-org-server hold" | sudo dpkg --set-selections && echo "mongodb-org-shell hold" | sudo dpkg --set-selections && echo "mongodb-org-mongos hold" | sudo dpkg --set-selections && echo "mongodb-org-tools hold" | sudo dpkg --set-selections`

You can verify that Mongo installed by using `mongo --version`

## Start and Enable MongoDB
Since we just installed it, we'll need to start the MongoDB service

* `sudo systemctl start mongod`
* `sudo systemctl enable mongod`
* `sudo systemctl status mongod`

## Access Control Setup
MongoDB **does not** come with basic authentication out of the box, so we **need** to enable access control to protect our data.

We'll start by starting Mongo and setting up an admin user:

* `mongo --port 27017`

Then in the Mongo shell:

* `use admin`

and paste the following, changing the password to whatever you like. Just be sure to keep track of it.

```
db.createUser({
    user: "admin",
    pwd: "admin123",
    roles: [ { role: "root", db: "admin" }]
})
```

You can then make sure your user was created using `db.getUsers()`, and then exit the mongo shell using the `exit` command.

Now navigate to the mongo config file to enable access control:

* `sudo nano /etc/mongod.conf`

In the file, find and uncomment the `security:` block and then add a line so it looks like the following:

```
security:
  authorization: enabled
```

Save the file and restart Mongo with

* `sudo systemctl restart mongod`

Now you will be using the following command to connect to the database with the admin account:

* `mongo --port 27017 -u "admin" -p "admin123" --authenticationDatabase "admin"`

Now that you have Mongo set up, you can refer to this [MongoDB command cheat sheet](https://dzone.com/articles/mongodb-commands-cheat-sheet-for-beginners) and the [Official MongoDB documentation](https://docs.mongodb.com/) for further information about creating a new database, setting up a user to access that database, and Mongo queries.

This marks the end of almost all the setup tutorials. 

* If you are configuring your actual development environment, please be sure you follow the tutorial for multiple users to ensure that your team can transition between roles smoothly.

## Multiple Users

* [Multiple Users Tutorial](/devops/more/final-setup.md)