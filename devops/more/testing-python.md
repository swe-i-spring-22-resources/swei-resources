# Running a Test Python Service
This tutorial will cover an example of setting up an actual service to run on our server to ensure that Nginx is working correctly as a reverse proxy.

To start, navigate to your repos location (`/home/teamXX/repos` if you followed previous tutorials) and run the following command to clone a simple test service that I created.

* `git clone https://github.com/krohwer/python-test-service.git`

Now, you'll need to navigate into the folder and set the ownership and permissions of the file to your systemd user:

* `cd python-test-service`
* `sudo chown systemduser:systemduser testService.py`
* `sudo chmod 500 testService.py`

## systemd
Next, create a systemd service file in order to run your service as a daemon anytime your server starts.

* `cd /etc/systemd/system`
* `sudo nano pythonTest.service`

Inside the file, paste the following, remembering to replace `teamXX` with the actual name of the directory you created:

```ini
[Unit]
Description=A service to test that my server is configured
After=syslog.target

[Service]
User=systemduser
ExecStart=/usr/bin/python3 /home/teamXX/repos/python-test-service/testService.py
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

Save the file, then start and enable your service:

* `sudo systemctl start pythonTest.service`
* `sudo systemctl enable pythonTest.service`
* `sudo systemctl status pythonTest.service`

## Nginx
Now we have to create a location block in our nginx config in order to direct to that service.

Go to your Nginx config:

* `cd /etc/nginx/sites-available`

And edit the file for your domain, replacing `teamXX` with your actual team number:

* `sudo nano teamXX.sweispring22.tk`

Inside the first server block of the file and below the default location block (`location / { }`) paste the following location block:

```
location /python {
   proxy_set_header Host $host;
   proxy_set_header X-Real-IP $remote_addr;
   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
   proxy_pass http://localhost:8082;
}
```

Now, the total file should look something like this:

```
server {

        root /var/www/teamXX.sweispring22.tk/html;
        index index.html index.htm index.nginx-debian.html;

        server_name teamXX.sweispring22.tk www.teamXX.sweispring22.tk;

        location / {
                try_files $uri $uri/ =404;
        }

        location /python {
           proxy_set_header Host $host;
           proxy_set_header X-Real-IP $remote_addr;
           proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
           proxy_pass http://localhost:8082;
        }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/ta.sweispring22.tk/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/ta.sweispring22.tk/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = ta.sweispring22.tk) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


listen 80;
        listen [::]:80;

        server_name ta.sweispring22.tk www.ta.sweispring22.tk;
    return 404; # managed by Certbot


}
```

Finally, restart Nginx:

* `sudo systemctl restart nginx`

If all was configured correctly, you should now be able to test your Python service in your browser by going to your domain and adding `/python` to the address, like so:

* `teamXX.sweispring22.tk/python`

## DevOps Home

* [Return to Main Page](/devops/devops.md)