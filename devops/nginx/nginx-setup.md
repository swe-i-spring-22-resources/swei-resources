# Nginx Setup
Nginx will be our webserver because it allows us to reroute HTTP requests to local services listening on a particular port, or even reroute traffic to other webservers entirely in addition to serving static resources (web pages). This is important because it allows us to handle HTTPS requests with Python code.

## Important Resources
The downside of Nginx is the complexity. Here are some important online resources for Nginx.

* [Serving Static Content with Nginx](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/)
* [Configuring Nginx as a Reverse Proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)

## Installation
The first step is to make sure the package manager is up to date and install Nginx:

* `sudo apt update`
* `sudo apt install nginx-full`

Then you’ll want to check the version and status to make sure it installed correctly:

* `nginx -V`
* `systemctl status nginx`

If the version returned is 1.18 or higher, you can then test the install by visiting the server IP address, which can be obtained by running the following command on your cloud and pasting the resulting ip address in your browser’s address bar:

* `curl -4 icanhazip.com`

You should be greeted with a landing page that looks like the following:

![Nginx Confirmation](https://i.imgur.com/NkyhavZ.png)

## Configuration
Moving forward, there are two important directories for configuring Nginx:

* `/etc/nginx/sites-available/`
* `/etc/nginx/sites-enabled/`

`Sites-available` is where we will create and edit configurations, and then they can be linked to `sites-enabled` when ready to deploy. However, before we can create a configuration, we need to set up a directory to put our front end html files. 

For right now, we will create a temporary path for these. In the future they will be directed to the Git repositories in the folder created during the [Git setup](/devops/python/python-setup.md).

To start, use the following commands to create the directory, set the owner, and set the permissions so that everyone can read and execute, but only the owner can write. Remember to replace “your_domain” with the actual domain for the server you are configuring from this point forward. Your actual domain is what you use to `ssh` into the server, and should be similar to `teamXX.sweispring22.tk`.

* `sudo mkdir -p /var/www/your_domain/html`
* `sudo chown -R $USER:$USER /var/www/your_domain/html`
* `sudo chmod -R 755 /var/www/your_domain`

Now we just want to create a simple html file to look for when we visit our domain later.  You can put whatever simple html you would like in this file:

* `nano /var/www/your_domain/html/index.html`

Now that we have a file to go to, we can set up our Nginx server block to direct users to the page.  We will be creating this file in the “`sites-available`” directory mentioned earlier.

* `sudo nano /etc/nginx/sites-available/your_domain`

And paste the following inside the file, again replacing your_domain with your actual domain:
```
server {
listen 80;
    	listen [::]:80;
 
    	root /var/www/your_domain/html;
    	index index.html index.htm index.nginx-debian.html;
 
    	server_name your_domain www.your_domain;
 
    	location / {
            	try_files $uri $uri/ =404;
    	}
}
```
### Note:
These configurations can very confusing and difficult to work with, so I recommend reading and bookmarking [this article](https://www.digitalocean.com/community/tutorials/understanding-nginx-server-and-location-block-selection-algorithms) to refer back to, as well as checking the official Nginx documentation that I linked at the top of this page. These should help you better understand the configurations so you can get them working as desired.

Here is a quick rundown on how it searches for files, as this is one of the more confusing aspects:
```
root = "/var/www/your_domain/html"
your domain = "your_domain.com"

User enters the address "http://your_domain.com/demand/order.html"

Nginx searches the root directory for "/demand/order.html" meaning it will be looking for a file called "order.html" at the location
"/var/www/your_domain/html/demand/"
```

Now that we have a server configuration, we can link this configuration to `sites-enabled` and test it.  If the test does not pass, make sure you have followed all the steps correctly and possibly ask for assistance if you cannot figure out the issue.

* `sudo ln -s /etc/nginx/sites-available/your_domain /etc/nginx/sites-enabled/`
* `sudo nginx -t`
* `sudo systemctl restart nginx`

If all is successful, you should be able to navigate to the domain that I have provided you and see your test page hosted on your cloud! Keep in mind the location of this configuration file, as you will need to access it later in order to configure routes to additional pages and resources.

As mentioned previously, the current location for static HTML files is temporary. When you have repository with your actual front end code, you will need to rewrite your server block and your server block so it points to the appropriate repo rather than the `/var/www/...` path.

## Setting up HTTPS
Next, we want to setup Letsencrypt with Certbot to get an SSL certificate and setup HTTPS.  Before beginning, make sure you have Nginx installed and have a server block configured and working.  Then we can install Certbot and get our SSL certificate with:

* `sudo apt install certbot python3-certbot-nginx`
* `sudo certbot --nginx -d your_domain`

You will be prompted to enter an email address for verification, asked to accept the terms and conditions, and asked if you want to join the mailing list.  Enter a valid email, accept the terms and conditions, and you can say no to the mailing list. Then you will be asked to choose if you want HTTP traffic redirected to HTTPS. **Pick option 2 to redirect traffic to HTTPS.**

If everything is successful, you should be told where the certificates are stored, and your server configuration should have a few lines managed by Certbot that will redirect HTTP to HTTPS.

### Important Note:
In my experience, messing with server blocks and Nginx configurations without a lot of experience can lead to a lot of errors and not many helpful error messages. The documentation I have provided at the top of this document may help with some, and I will do my best to provide more helpful resources throughout the project.

## Configuring Nginx as a Reverse Proxy
Right now, Nginx is only set up to serve static pages. Eventually, we will want it to be able to forward HTTP requests to our Python services running on specific ports. 

To do this, you simply need to add a location block that directs to the appropriate endpoint, and add in code so it looks like the following block:

```
location /backend-endpoint/ {
   proxy_set_header Host $host;
   proxy_set_header X-Real-IP $remote_addr;
   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
   proxy_pass http://localhost:8080;
}
```

In this example, if you want to do a login request, the front end would send the request to `https://your_domain.com/backend-endpoint/login`, the request would be routed to the port `8080`, and then a backend service listening on that port would receive the request, and process the login endpoint.

You will need to coordinate both the endpoint and the port with your Full Stack Engineer or whoever is writing your back end services, as well as whoever is working on the front end so they can send the HTTP requests to the proper endpoint.

## Testing a Basic Service
If you're configuring for the first time and you just want to make sure systemd, nginx, and python are playing nicely, follow this tutorial to deploy a simple service and test it from your browser.

* [Deploying a Python Test Service](/devops/more/testing-python.md)

## Databases
Now that we've got a functional webserver, we can set up our database of choice.

* [MySQL Setup](/devops/database/mysql-setup.md)
* [MongoDB Setup](/devops/database/mongo-setup.md)