# Python Setup
Since you will be using a python backend for this project, it's important to make sure Python is properly configured on your cloud. This tutorial will walk you through the required steps to set up a Python environment.

While logged into the server using the user account you have created, run the following commands:

* `sudo apt update && sudo apt upgrade`
* `python3 -V    	## Check that Python 3 is installed`

Install pip, a python package manager:

* `sudo apt install -y python3-pip`

This can be used to install any packages we need with the command:

* `pip3 install package_name`

And finally, install some additional development tools:

* `sudo apt install -y build-essential libssl-dev libffi-dev python3-dev`
* `sudo apt install -y python3-venv`

Now that python is setup, we want to create a python environment to isolate each project using the following commands:

* `cd ~`
* `mkdir environments`
* `cd environments`
* `python3 -m venv your_environment_name`

This new environment can be activated with the command:

* `source environments/your_environment_name/bin/activate`

We can automate this activation by adding that same command to the end of your user’s `.profile`, a file that runs scripts every time you log in. This can be edited with:

* `sudo nano ~/.profile`

When the environment is successfully activated, you should see its name to the left of your name in the terminal. If this is the case, python is now properly set up in your cloud environment.

# Git Setup
Since your team will be collaborating on this project and multiple team members will be deploying code, it is important to set up a Git directory where all of your repositories will be cloned.

To start, ensure that your environment has Git installed by using the command:

* `git --version`

Once you have verified that you have a version of Git installed, you should create a directory for your repositories off of your home directory, so it is accessible by all users. I would recommend naming the directory after your team, like teamXX (but use your actual team number of course):

* `cd /home`
* `sudo mkdir directory-name`
* `sudo chown -R <your_user>:<your:user> directory-name`

Then, inside your team folder add a `repos` directory

* `cd teamXX`
* `sudo mkdir repos`
* `sudo chown -R <your_user>:<your:user> repos`

Now, all of your code repositories can be cloned into the directory by navigating into `/home/teamXX/repos` and using the command

* `git clone <git-url>`

It is strongly recommended that your entire team review Git if you are not familiar, as it is essential for this project and very easy to mess up. You can read the [official Git documentation](https://git-scm.com/docs) for more information.

## Systemd
The next step is setting up systemd in order to run some of our python code as a background service.

* [Systemd Setup](/devops/systemd/systemd-setup.md)