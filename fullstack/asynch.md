# Asynchronous Calls
For the purposes of this resource, the main thing you need to understand about asynchronous calls is that they allow us to make HTTP requests. Of course, we encourage you to explore more about its uses on your own.

Here we are providing code snippets of a few ways you can create an HTTP request. The source used for these can be found [here](https://www.freecodecamp.org/news/here-is-the-most-popular-ways-to-make-an-http-request-in-javascript-954ce8c95aaa/). If you want to know which method we'd encourage you to explore specifically, `fetch` and its concept of promises is a useful tool to learn. However, you're free to use whatever method is most comfortable for your team.

## Table of Contents
* [AJAX](#ajax)
* [jQuery](#jquery)
* [fetch](#fetch)

### AJAX
`AJAX` is the traditional method to make an HTTP request with the use of `XML`.

```javascript
const http = new XMLHttpRequest();
const url = "https://your-url-here.com";
http.open("GET", url);
http.send();

http.onreadystatechange=function(){
    if (this.readyState==4 && this.status==200 ){
        console.log(http.responseText);
    }
}
```

We've provided a `"GET"` request example here which might not be all that useful for this project, so we recommend you view the documentation via [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest) for additional information to perform this request.

### jQuery
`jQuery` has one of the easiest ways to conduct an HTTP request.

Please note that you *need* to import the following library for the call to function.

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
```

From here, you can use the `$.ajax` method to conduct your HTTP request.

```javascript
const requestUrl = "https://your-url-here.com";
var info = { 'key' : "value" };

$.ajax({
    url: requestUrl,
    type: "POST",
    data: JSON.stringify(info),
    success: function(result){
        console.log(result)
    },
    error: function(error){
        console.log(`Error ${error}`)
    }
})
```

There are various ways to go about this, so we recommend you look at the source article for simple starter templates as well as [jQuery AJAX](https://api.jquery.com/jquery.ajax/) for a more detailed and useful source to use it in a more robust and complex way.

### fetch
`fetch` is one of the newest and widely preferred method of creating a modern HTTP request. It revolves around the idea of a `promise` to manage a response.

```javascript
const url = "https://your-url-here.com";
var info = { 'key' : "value" };

const params = {
    headers: {
        "content-type" : "application/json; charset=UTF-8"
    },
    body: JSON.stringify(info),
    method: "POST"
}

fetch(url, params)
.then(data => {return data.json()})
.then(result => {console.log(result)})
.catch(error => console.log(error))
```

`fetch` is a very robust and popular tool for a reason. Be sure to review the documentation from [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) for more information. Also note that this example makes use of arrow functions, so be sure to look into those if that portion of the example is confusing.