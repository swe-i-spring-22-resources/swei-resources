# Writing a Back-End Python Service
In this tutorial, I'll be covering the basic information that you need in order to write a Python file that can be run as a service on your server.

These types of files are responsible for receiving HTTP requests that are forwarded to them by your web application server (Nginx in this case), processing the information, and interacting with your database. These services are essentially the `main.py` of your system.

In order to handle HTTP requests, we'll be using the `BaseHTTPRequestHandler` from the `http.server` python library in order to "listen" for HTTP requests that match a specific hostname and port. The documentation for this library can be found [here](https://docs.python.org/3/library/http.server.html).

## Setup
Here are the imports that we'll be using to write these services:

```Python
import logging
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
from http import HTTPStatus
from urllib.parse import urlparse, parse_qs
```

The first thing you'll want to do is set up logging for debugging purposes. To do this, simply add the following line just below your imports:

```python
logging.basicConfig(filename='example.log', encoding='utf-8', level=logging.DEBUG)
```

This will specify a file that the program will log information to. You can use this by adding log statements whenever you have information that you'd like to report to your log. You can find more info about the logging library [here](https://docs.python.org/3/howto/logging.html).

## The Service Class
The first part of writing a Python service is creating a class that inherits from `BaseHTTPRequestHandler` to house your endpoints and request handling logic. You'll want to give this class an appropriate name based on the purpose of the service. For this tutorial, we'll call it BaseAppService. Below is a sample declaration of the class with proper inheritance.

```python
class BaseAppService(BaseHTTPRequestHandler):
```

Inside the class, you can define an HTML response code dictionary. This will allow you to easily grab response codes and use them later when generating responses.

```python
# HTTP Response code dictionary
# Add any additional response codes you might want to use
# You can look up the codes and their proper uses online
HTTP_STATUS_RESPONSE_CODES = {
    'OK': HTTPStatus.OK,
    'FORBIDDEN': HTTPStatus.FORBIDDEN,
    'NOT_FOUND': HTTPStatus.NOT_FOUND,
}
```

Next, you'll need to add two functions for extracting POST and GET data from the requests so you can process the data sent. Below are some sample functions that you are welcome to use to accomplish this.

```python
# Here's a function to extract GET parameters from a URL
def extract_GET_parameters(self):
    # GET parameters are just stored in the path of the request, so we can just urlparse this
    path = self.path
    parsedPath = urlparse(path)
    paramsDict = parse_qs(parsedPath.query)
    logging.info('GET parameters received: ' + json.dumps(paramsDict, indent=4, sort_keys=True))
    return paramsDict

# Here's a function to extract POST parameters from a POST request
def extract_POST_Body(self):
    # The content-length HTTP header is where our POST data will be in the request. So we'll need to
    # read the data using an IO input buffer stream built into the http.server module.
    postBodyLength = int(self.headers['content-length'])
    postBodyString = self.rfile.read(postBodyLength)
    postBodyDict = json.loads(postBodyString)
    logging.info('POST Body received: ' + json.dumps(postBodyDict, indent=4, sort_keys=True))
    return postBodyDict
```

## Handling GET Endpoints
Now that you've got the necessary functions defined, you'll need to write a function to actually handle the types of requests and set the endpoints to determine what code is executed based on the address/path used. We'll start with the `do_GET` function to handle GET endpoints.

```python
def do_GET(self):
    # self.path here will return us the http request path entered by the client.
    path = self.path
    # Extract the GET parameters associated with this HTTP request and store them in a python dictionary
    paramsDict = self.extract_GET_parameters()
    status = self.HTTP_STATUS_RESPONSE_CODES['NOT_FOUND']
    responseBody = {}

    # This is a root URI or block of code that will be executed when client requests the address:
    # http://localhost:8082.
    if path == '/':
        status = self.HTTP_STATUS_RESPONSE_CODES['OK']
        responseBody['data'] = 'Hello world'

    # Here is where you define your GET endpoints
    # Note that for GET, you should use the "in" format to ensure you still get the correct endpoint
    # even when parameters are included
    elif '/getCurrentTemp' in path:
        status = self.HTTP_STATUS_RESPONSE_CODES['OK']

        # This is where you would process GET data and prepare your response
        # Note that you likely won't be using GET endpoints here since most of the data you will
        # be using should be sent over POST
        currentTemperatureData = 100.0

        responseBody['data'] = currentTemperatureData

    # If you want to add more endpoints, simply add more elif statements with the endpoints
```

Note that within each endpoint is where you will actually run the code that you want to execute such as any processing/calculations and database interactions.

## Handling POST Endpoints
Similar to GET endpoints, we should also define a function to handle POST endpoints. This is probably the more common section you'll be using as most of your data is better transmitted in POST.

```python
def do_POST(self):
    path = self.path
    # Extract the POST body data from the HTTP request, and store it into a Python
    # dictionary we can utilize inside of any of our POST endpoints.
    postBody = self.extract_POST_Body()
    status = self.HTTP_STATUS_RESPONSE_CODES['NOT_FOUND']

    responseBody = {}
    # These path endpoints can be very picky sometimes, and are very much connected to how your DevOps
    # has configured your web server. You will need to communicate with your DevOps to decide on these
    if path == '/getHourlyForecast':
        # This is where you'll implement your actual service logic
        # Process any POST data you are expecting to receive
        hourlyForecastData = [["2021-01-06 18:00:00-06:00", 274.8], ["2021-01-06 18:00:00-07:00", 212.4]]

        # Collect any data you want to send back into a dictionary and send it back in the responseBody
        responseBody['data'] = hourlyForecastData
```

Same as with GET endpoints, within each endpoint is where you will do your data processing and database interactions.

## Sending a Response
Now that you've got your request endpoints handled and a response generated, within each of those previous functions you'll need to properly format and send an HTTP response back to the client.

The following sample code documents each necessary call and explains the function.

```python
# This will add a response header to the header buffer. Here, we are simply sending back
# an HTTP response header with an HTTP status code to the client.
self.send_response(status)
# This will add a header to the header buffer included in our HTTP response. Here we are specifying
# the data Content-type of our response from the server to the client.
self.send_header("Content-type", "text/html")
# The end_headers method will close the header buffer, indicating that we're not sending
# any more headers back to the client after the line below.
self.end_headers()
# Convert the Key-value python dictionary into a string which we'll use to respond to this request
# When using the json.dumps() method, you may encounter data types which aren't easily serializable into
# a string. When working with these types of data you can include an additional parameters in the dumps()
# method, 'default=str' to let the serializer know to convert to a string when it encounters a data type
# it doesn't automatically know how to convert.
response = json.dumps(responseBody, indent=4, sort_keys=True, default=str)
logging.info('Response: ' + response)
# The wfile.write() method will only accept bytes data, so convert your response to bytes
byteStringResponse = response.encode('utf-8')
self.wfile.write(byteStringResponse) # then this will send it back to the user
```

## Running the Application
The last step is to set up an entrypoint to actually run your service and make it listen for requests on a specific port. You'll need to coordinate the port with DevOps so that they can configure Nginx to forward the requests to the proper location.

The following code is a sample main function that is listening for requests on the port 8082, and setting up `BaseAppService` as the app server.

```python
if __name__ == '__main__':
    hostName = "localhost"
    # Communicate with your DevOps to pick a port for each of your services.
    # Note that ports 0-1023 are reserved and cannot be used.
    serverPort = 8082
    appServer = HTTPServer((hostName, serverPort), BaseAppService)
    logging.info('Server started http://%s:%s' % (hostName, serverPort))

    # Start the server and fork it. Use 'Ctrl + c' command to kill this process when running it in the foreground
    # on your terminal.
    try:
        appServer.serve_forever()
    except KeyboardInterrupt:
        pass

    appServer.server_close()
    logging.info('Server stopped')
```

The entire example service is available in the BitBucket if you need to see it all put together.