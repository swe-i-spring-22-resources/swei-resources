# Full Stack Resources
The role of a full stack engineer is to manage databases, clients, and servers. This involves creating the browser the user will interact with, the server that manages the API calls within your system, and managing the creation and retrieval of data within a database. You can learn more about Full Stack engineers [here](https://youtu.be/8KaJRw-rfn8) if you wish to have more of an overview.

In terms of this project, Full Stack is a very essential role in designing and coding your platform. The deployment of this code falls on the DevOps member(s) of your team, so be sure to stay in touch with them to help deploy and troubleshoot your code when it's live on your domain. There is minimal to average use of the command line based on your understanding and interaction with [git](/vcs/git.md), but your main focus will be with IDEs.

These resources are meant to introduce you to useful tools and starting points to assist the development of your project. Some self research and experimentation will be necessary to see your project come to fruition, so don't be afraid to mess around with it.

#### Please note: you are *not* permitted to use front-end and backend frameworks in this project.
You will work with HTML, CSS, and JavaScript for the front-end.

* For our purposes, we are defining front-end frameworks as the following examples: React, Angular, etc.
* Also for our purposes, we are defining front-end libraries as the following examples: jQuery, Bootstrap, etc.

Front-end libraries *are* allowed to be used. If you are unsure if you can use a specific tool or not, be sure to ask.

You will work with Python for the backend.

* Similarly, backend frameworks such as Django, Flask, etc. are not allowed.

***However***, during Sprint 2, your team will have the option of documenting and making a decision matrix for the use of Flask in Sprint 3 and onward in the project. For Sprint 2 though, everyone will begin to work off of the same base file we provide.

Failure to stay within our permitted guidelines will result in poor TA Checkpoint scores, and the potential of us being limited in the way we can help you.

## Table of Contents
* [General Front-end Resources](#general-front-end-resources)
* [Asynchronous Calls](/fullstack/asynch.md)
* General Backend Resources (Coming Soon)
    * [Writing a Back-End Service](/fullstack/back-end/services.md)

### General Front-end Resources
Don't worry if you need to brush up on your web development knowledge! I highly recommend you review your notes from Web Programming if you ever need a starting point.
You can also have a look at the following sites for additional help:

* [W3Schools](https://www.w3schools.com/)
* [MDN Web Docs](https://developer.mozilla.org/en-US/)

W3Schools has code snippets and introductions to various structures and functions that are available for you to use. Unfortunately, W3Schools is often noted to be outdated, so if you begin there to explore your options, be sure to see the full and modern application of such features in MDN Web Docs.
MDN Web Docs, which we will often refer to as Mozilla Docs, will provide in-depth examples, documentation, and browser compatibility for various functionality that you will likely learn and apply in your project. If you're working on the front-end, definitely bookmark this website for reference! It is absolutely the *best* source for front-end web development.

Sometimes CSS and styling for your website can be a bit tedious to fulfill. We do *not* recommend you pull any random templates online and work from there though (please provide credit as necessary if you do this). There are plenty of libraries that can help with this aspect of your project:

* [Bootstrap](https://getbootstrap.com/)
* [Font Awesome](https://fontawesome.com/)

Bootstrap provides pre-defined styling and responsive tools to give you an easier foundation to design your website from. As stated earlier, we do *not* encourage the use of templates, and that remains in place with this tool. However, we *do* encourage the use of its CSS and or JS components.

Font Awesome provides vector icons that you can directly import and or download for use in your project. At least one person on the team will need to register an account with this tool, but nothing else is required to use the icons for free. It definitely helps if there are graphic design elements you wish to include in your project, but don't have the skill and or time to execute.
