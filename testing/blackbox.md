# Black Box Testing
Black Box Testing is the testing of software without knowledge of its internal components. The focus is the user input and the expected output from the software. These are tests that should be able to be conducted by someone who has never interacted with your software and or its code before.

For this course, Black Box Testing will focus on functional and regression testing. Some introduction to non-functional black box testing will appear towards the end of the project, if your team has the availability to do so.

## Table of Contents
* [Functional Testing](#functional-testing)
    * Selenium (Coming Soon)
* [Regression Testing](#regression-testing)

### Functional Testing
As implied by the name, functional black box testing is meant to test the functional requirements of a system. One way to prepare this type of test is to set up a detailed set of instructions for testers to follow. A set of instructions should ultimately make up a `path` in interacting with your system. For simplicity sake, we'll have a main `Happy Path`, 0 or more `Alternative Path(s)`, and `Exception` path(s).

As an example, let's assume the following links are our "system" and create some paths to interact with them.

[Click Here](https://google.com)

[Don't Click Here]()

##### Happy Path
```
Step 1: user clicks on the link labeled 'Click Here' => is redirected to google.com
```

The `Happy Path` is the main way the user is expected to interact with the system and get its intended response. In this example, we wanted the user to click on the link labeled `Click Here` and be redirected to Google's main page. An example in your system would be the set of instructions for the user to successfully log in.

##### Alternative Path(s)
An `Alternative Path` would highlight a different set of instructions that would still result in the same end goal as the `Happy Path`. A potential example in your system would be (not) filling in an optional field when registering an account.

##### Exceptions
```
Step 1: user clicks on the link labeled 'Don't Click Here' => page refreshes.
```

An `Exception` path would highlight the set of instructions of a user using invalid input within your system. In this example, if the user clicked on the link labeled `Don't Click Here`, the page refreshes. An example in your system would be a user submitting an empty form when logging in or registering. If your site handles invalid interactions with a popup alert or anything else, document the expected outcome as such.

As implied by the examples, additional steps will increment the step counter. This continues until the tester reaches the end of that path of testing. For example, the final step of a user logging in to your system could end like the following:

```
Step n: user clicks "Log In" button => user is redirected to personal customer dashboard page.
```

The necessary tests for a complex system can be very tedious to document and manually follow. Later in the project, you will be introduced to an automated black box testing tool by the name of Selenium. We encourage you to not worry about this for the time being. Introductory documentation will be provided when the tool is introduced in lecture.

### Regression Testing
Regression testing is rather straightforward. If any code is updated, or any other form of system maintenance occurs, you would retest the software to ensure it's still working as intended. Essentially, by preparing and setting up your (non) functional black box tests, you are simultaneously setting up your regression black box tests.
