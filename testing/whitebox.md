# White Box Testing
White Box Testing tests the internal structure and design of the software. Since this form of testing involves seeing everything about the system, it is often also known as Clear Box Testing. For this project, we will primarily focus on Unit Testing to fulfill this, which means your backend will need to use an OOP (Object-Oriented Programming) structure.

This document will provided a general overview of things to keep in mind when structuring and creating Python unit tests. If you would prefer a video tutorial and demonstration of a Python unit test, consider checking out the following video:

* [Python Tutorial: Unit Testing Your Code with the unittest Module](https://youtu.be/6tNS--WetLI) by Corey Schafer

## Table of Contents
* [General Setup](#general-setup)
* [General Testing](#general-testing)
* [Mock](#mock)

### General Setup

Unsurprisingly, the `unittest` module is necessary to conduct the unit testing of your project. The documentation of this module is linked below:

* [unittest](https://docs.python.org/3/library/unittest.html)

When creating your unit testing file, it is highly recommended to name it using the following format:

```
test_test_name_here.py
```

It is the standard naming convention to begin your unit test files with `test_`.

To begin unit testing, you must first import the unittest module and the class that you will be testing. You must also create a test class that inherits from `unittest.TestCase`.

```python
import unittest
from class_to_test import ClassToTest

class TestClassToTest(unittest.TestCase)
```

Within this test class is where you will code your test cases. Please note that all defined methods *must* begin with `test_`, otherwise the method will not run. An example is provided below.

```python
class TestClassToTest(unittest.TestCase)

    def test_someFunction(self)
        ...
```

Once you've set up your test methods, you might want to run your code to make sure it's all working. In order to do so, include the following code at the bottom of your unit test file *outside* of your test class. This snippet allows you to run the file in your IDE and makes it easier to run through the command line.

```python
class TestClassToTest(unittest.TestCase)
    ...

if __name__ == '__main__':
    unittest.main()
```

If you find yourself using the same instance variables throughout your test cases, use the `setUp` and `tearDown` methods to create them all in one spot. This allows you to manage your code in a more convenient way.

```python
class TestClassToTest(unittest.TestCase)

    def setUp(self):
        ...
    
    def tearDown(self):
        ...

```

**Notice:** these methods should be called before your test cases. It's also worth mentioning that everything in `setUp` will run before every test case, while everything in `tearDown` will run after every test case.

A final overview of the recommended structure of your unit test file is provided below.

```python
import unittest
from class_to_test import ClassToTest

class TestClassToTest(unittest.TestCase)

    def setUp(self):
        ...
    
    def tearDown(self):
        ...
    
    def test_someFunction(self):
        ...
    
    def test_someOtherFunction(self):
        ...
    
    ...

if __name__ == '__main__':
    unittest.main()
```

Now you're ready to set up your unit tests!

### General Testing

The following methods should be used when writing your unit tests, where `a` and `b` are compared values. Check the `unittest` documentation provided earlier to see other available methods and explanations on how they can be used.

```python
self.assertEqual(a, b)

self.assertNotEqual(a, b)

self.assertTrue(a, b)

self.assertFalse(a, b)

self.assertIsNone(a, b)

self.assertIsNotNone(a, b)
```

### Mock
Let's say your code requires some sort of API call to function. The risk of this is that your tests will rely on the website to be up in order to function. Similarly, having code that's dependent on a database can also raise problems when testing.

Thankfully, the `mock` module provides a way to work around these concerns. The documentation of this module is linked below:

* [mock](https://docs.python.org/3/library/unittest.mock.html)

First, import the module.

```python
import unittest
from unittest.mock import patch
from class_to_test import ClassToTest
```

There are various ways of using mock as it depends on your use case and class structure. A simple `mock` of a `GET` request in a created module is displayed below.

```python
    def test_requestFunction(self):
        with patch('class_to_test.requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            ...
```

You can manipulate various aspects and expected outcomes with `mock`.

Things start getting more complicated when patching database calls. Below we have provided a source of mocking an entire database if your structure allows for it.

* [Python Testing With a Mock Database](https://medium.com/swlh/python-testing-with-a-mock-database-sql-68f676562461) by Minul Lamahewage

If you don't seem to be getting anywhere due to database connections, feel free to patch out the entire function itself. This will result in a lower test coverage, but is at least a start to have *some* sort of coverage at all.
