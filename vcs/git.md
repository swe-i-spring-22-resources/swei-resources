# Introduction to Git

This resource is intended to provide the basics in using and understanding git. There are plenty of sources online for help, but hopefully this can act as an easy to find cheat sheet to not ruin your workflow. Remember to first download git before attempting to use it:

* [Git download for Windows, Mac, & Linux](https://git-scm.com/downloads)

You can then use the command `git --version` to make sure Git installed properly.
Also be sure to review your respository's README to understand the VCS plan necessary to properly contribute to it.
This resource assumes you know how to use the command line to navigate through your directory. I will only provide commands/instructions specifically related to git and BitBucket.

If you like following a video and seeing this all explained and used in real time, be sure to check out the following playlist and video:

* [Git and GitHub for Poets Playlist](https://youtube.com/playlist?list=PLozRqGzj97d02YjR5JVqDwN2K0cAiT7VK) by The Coding Train
* [Introduction to Git - Branching and Merging](https://youtu.be/FyAAIHHClqI) by David Mahler

Please note that the playlist provided specifically uses GitHub to teach Git, but everything applies to BitBucket. You just navigate the site a bit differently. It's also worth noting that the playlist is *completely* for beginners. If you're already familiar with the basics of git and just wanted to touch up on it once again, the rest of this document and the video by David Mahler has you covered.
David Mahler covers some advanced concepts of git as well, so I recommend you ignore the merging portions of the video as you will be heavily encouraged to use Pull Requests on BitBucket to merge your branches. You can find information on this process below:

* [BitBucket Pull Requests](/vcs/pullrequests.md)

A final note, some IDEs like PyCharm and Visual Studio Code have UI tools to interact with git. If you're still struggling with the command line, I recommend you look into these tools and see if they work for you.

## Table of Contents
* [Repository Setup](#repository-setup)
* [Branching](#branching)
* [Commit, Push, and Pull](#commit-push-and-pull)
* [Merging](#merging)

### Repository Setup

As you will see throughout this document, all git commands begin with `git` and are followed with whatever command you need.

To begin, it's best for you to set up the information for your account. Since you are required to use BitBucket in this course, use your BitBucket credentials here. You can set your name and email of your account with the following commands:

```
git config --global user.name "Your name here"
git config --global user.email your-email@stedwards.edu
```

You should be prompted to log in to your BitBucket account if you attempt to do anything afterward.
**Note:** git may require you to set these credentials and or log in every time you restart/turn on your machine. If you're getting errors related to access, just redo this step to make sure your credentials are set.

If you haven't already, create a repository on BitBucket by accessing your workspace and selecting `Create repository`.

![Workspace Main Page with Highlighted Button](/vcs/screenshots/create-repo.PNG "Empty Workspace")

From here, you can create a repository. Make sure to select a project or create one on this page if you have no projects in your workspace already.
**Note:** I am not including a `.gitignore` for this example, but you should include one for all of your repositories.

![Repository Creation Example](/vcs/screenshots/repo-creation-example.PNG "Repository Creation Example")

To clone a repository to your local machine, go to BitBucket and click on the repository you wish to clone. SSH cloning is typically more secure to clone with (and is *required* for DevOps on the droplet), but for the purposes of this course, the rest of the team cloning with HTTPS to their local machine is just fine. You can copy, paste, and run the command provided in whatever directory you wish to have the repository in on your local machine.

![Repository Main Page with Highlighted Button](/vcs/screenshots/clone-repo.PNG "Newly Created Repository")

![BitBucket Clone Popup Window](/vcs/screenshots/clone-repo-window.PNG "Clone Repository Popup Window")

With this, you should be ready to work locally on your repository.

### Branching

To understand the structure and use of branching, I would once again highly recommend this [video](https://youtu.be/FyAAIHHClqI?t=152) by David Mahler. I have timestamped the video for your convenience. You have the ability to create branches locally on your machine or through BitBucket. I will show both methods here.

To create a branch thorugh BitBucket, click on the `Branches` tab in the repository you would like to create your branch on. On this screen, you will be able to see all active branches that are up on BitBucket. Similar to cloning a repository, you can click on `Create branch` button and interact with the popup window to create a new branch.

Use the following command to create a branch locally on your machine:

```
git branch new-branch-name-here
```

Use the following command to see which branch you're on:

```
git branch
```

Use the following command to switch to a different branch:

```
git checkout branch-name-here
```

### Commit, Push, and Pull

In order to share your code with your team, you must commit then push your files. You *cannot* push/upload your code onto BitBucket without committing your changes, so be sure to always commit first. Your IDE will have some form of indicator to show which files have been changed, which means they are staged and ready to be committed.

Use the following command to **commit** all of your files:

```
git commit -a -m "Write your commit message here"
```

`-a` means to add all staged/changed files, and `-m` will allow you to insert a message with the commit. **Note:** this command does *not* upload your changes to BitBucket. Only pushing your code will.

Use the following command to **push** all of your committed files to BitBucket:

```
git push origin branch-name-here
```

`origin` is the default remote of the repository. If you are having errors related to the remote, you may need to double check the remote of the repository and the remote you are accessing.
If you are pushing a locally created branch for the first time, you will get additional output to confirm that the local branch was created on your repository. I encourage you to verify that your files were uploaded to BitBucket after pushing.

Use the following command to **pull** changes that others have uploaded onto BitBucket:

```
git pull origin branch-name-here
```

Notice how similar the formatting is to a push.

### Merging

Since we are ***heavily encouraging*** the use of Pull Requests (PRs) to merge your branches to each other, I will **not** provide merging commands here. Use the following resource to learn how to conduct a PR on BitBucket:

* [BitBucket Pull Requests](/vcs/pullrequests.md)
