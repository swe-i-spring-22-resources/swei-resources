# BitBucket Pull Requests

Pull Requests are very important in VCS as they allow a team to review each other's code before merging. This is particularly necessary for important branches like `main`. It's fairly simple to create a Pull Request through BitBucket, no command line required!

First, navigate to the repository you need to conduct a Pull Request in, and click on the "Pull requests" tab on the left side of the repository.

![BitBucket Create Pull Request](/vcs/screenshots/create-pull-request.png "Create Pull Request")

From here, you will be met with currently open Pull Requests that need to be resolved. Next, click on the "Create pull request" button in the upper right hand corner.

From there, you will be met with a screen to create your Pull Request (PR).

First, you will want to select which branches are involved in the PR. The branch selected on the left contains all of the changes you are attempting to merge into the higher order branch, so in this example it's `development`. The branch on the right is the branch that will take in all of the changes, so in this example it's `main`.

Next, you can provide a descriptive title that summarizes the changes made in the branch.

Then, you can fill out the description. I have provided a simple "template" example below. BitBucket typically lists the pushed commits, but I recommend you listen them if BitBucket fails to automatically fill it out for whatever reason. PRs for backend repositories should include a `Test Coverage` section with the output from your unit test.

Finally, we recommend you list other team members as reviewers, preferably members that are familiar with or have worked on the repository. A member with administrative access on the workspace can list default reviewers if your team finds it necessary to do so.

![BitBucket Pull Request Form](/vcs/screenshots/pull-request-form.png "Pull Request Form")

With that, you can submit your PR for review. Do *not* check the "Close branch" checkbox at the end of the form unless you want to delete the branch you are merging from.

![BitBucket Complete Pull Request](/vcs/screenshots/complete-pull-request.png "Complete Pull Request")

Upon submitting a PR, it will be reviewed by another member on your team and be approved, receive comments for additional changes, or denied. Follow the VCS plan that your team has outlined to know what it takes for a PR to be approved.

It should go without saying, but you should ***not*** self-approve PRs unless absolutely necessary. Sometimes you just need to push code, and we understand that, but do your best to push code in a timely manner so your team understands what you are contributing to the project.
